#!/bin/python


"""
sudo apt-get install python3-pip python3-pandas python3-plotly -y
pip3 install plotly

"""

import os
import re
import json
import time
import ipaddress

from collections import Counter



import pandas as pd
import plotly.offline as py
import plotly.graph_objs as go



global logs, asnFile
#logsFile = 'access.log'
#asnFile = 'ip2asn-v4.tsv'
logsFile = os.environ['LOG_FILE']
asnFile = os.environ['IP2ASN']
entriesLimit = int(os.environ['Entries_Limit'])
timeSlep = int(os.environ['Time_Interval'])

IISO_Three = {"BD": "BGD", "BE": "BEL", "BF": "BFA", "BG": "BGR", "BA": "BIH", "BB": "BRB", "WF": "WLF", "BL": "BLM", "BM": "BMU", "BN": "BRN", "BO": "BOL", "BH": "BHR", "BI": "BDI", "BJ": "BEN", "BT": "BTN", "JM": "JAM", "BV": "BVT", "BW": "BWA", "WS": "WSM", "BQ": "BES", "BR": "BRA", "BS": "BHS", "JE": "JEY", "BY": "BLR", "BZ": "BLZ", "RU": "RUS", "RW": "RWA", "RS": "SRB", "TL": "TLS", "RE": "REU", "TM": "TKM", "TJ": "TJK", "RO": "ROU", "TK": "TKL", "GW": "GNB", "GU": "GUM", "GT": "GTM", "GS": "SGS", "GR": "GRC", "GQ": "GNQ", "GP": "GLP", "JP": "JPN", "GY": "GUY", "GG": "GGY", "GF": "GUF", "GE": "GEO", "GD": "GRD", "GB": "GBR", "GA": "GAB", "SV": "SLV", "GN": "GIN", "GM": "GMB", "GL": "GRL", "GI": "GIB", "GH": "GHA", "OM": "OMN", "TN": "TUN", "JO": "JOR", "HR": "HRV", "HT": "HTI", "HU": "HUN", "HK": "HKG", "HN": "HND", "HM": "HMD", "VE": "VEN", "PR": "PRI", "PS": "PSE", "PW": "PLW", "PT": "PRT", "SJ": "SJM", "PY": "PRY", "IQ": "IRQ", "PA": "PAN", "PF": "PYF", "PG": "PNG", "PE": "PER", "PK": "PAK", "PH": "PHL", "PN": "PCN", "PL": "POL", "PM": "SPM", "ZM": "ZMB", "EH": "ESH", "EE": "EST", "EG": "EGY", "ZA": "ZAF", "EC": "ECU", "IT": "ITA", "VN": "VNM", "SB": "SLB", "ET": "ETH", "SO": "SOM", "ZW": "ZWE", "SA": "SAU", "ES": "ESP", "ER": "ERI", "ME": "MNE", "MD": "MDA", "MG": "MDG", "MF": "MAF", "MA": "MAR", "MC": "MCO", "UZ": "UZB", "MM": "MMR", "ML": "MLI", "MO": "MAC", "MN": "MNG", "MH": "MHL", "MK": "MKD", "MU": "MUS", "MT": "MLT", "MW": "MWI", "MV": "MDV", "MQ": "MTQ", "MP": "MNP", "MS": "MSR", "MR": "MRT", "IM": "IMN", "UG": "UGA", "TZ": "TZA", "MY": "MYS", "MX": "MEX", "IL": "ISR", "FR": "FRA", "IO": "IOT", "SH": "SHN", "FI": "FIN", "FJ": "FJI", "FK": "FLK", "FM": "FSM", "FO": "FRO", "NI": "NIC", "NL": "NLD", "NO": "NOR", "NA": "NAM", "VU": "VUT", "NC": "NCL", "NE": "NER", "NF": "NFK", "NG": "NGA", "NZ": "NZL", "NP": "NPL", "NR": "NRU", "NU": "NIU", "CK": "COK", "XK": "XKX", "CI": "CIV", "CH": "CHE", "CO": "COL", "CN": "CHN", "CM": "CMR", "CL": "CHL", "CC": "CCK", "CA": "CAN", "CG": "COG", "CF": "CAF", "CD": "COD", "CZ": "CZE", "CY": "CYP", "CX": "CXR", "CR": "CRI", "CW": "CUW", "CV": "CPV", "CU": "CUB", "SZ": "SWZ", "SY": "SYR", "SX": "SXM", "KG": "KGZ", "KE": "KEN", "SS": "SSD", "SR": "SUR", "KI": "KIR", "KH": "KHM", "KN": "KNA", "KM": "COM", "ST": "STP", "SK": "SVK", "KR": "KOR", "SI": "SVN", "KP": "PRK", "KW": "KWT", "SN": "SEN", "SM": "SMR", "SL": "SLE", "SC": "SYC", "KZ": "KAZ", "KY": "CYM", "SG": "SGP", "SE": "SWE", "SD": "SDN", "DO": "DOM", "DM": "DMA", "DJ": "DJI", "DK": "DNK", "VG": "VGB", "DE": "DEU", "YE": "YEM", "DZ": "DZA", "US": "USA", "UY": "URY", "YT": "MYT", "UM": "UMI", "LB": "LBN", "LC": "LCA", "LA": "LAO", "TV": "TUV", "TW": "TWN", "TT": "TTO", "TR": "TUR", "LK": "LKA", "LI": "LIE", "LV": "LVA", "TO": "TON", "LT": "LTU", "LU": "LUX", "LR": "LBR", "LS": "LSO", "TH": "THA", "TF": "ATF", "TG": "TGO", "TD": "TCD", "TC": "TCA", "LY": "LBY", "VA": "VAT", "VC": "VCT", "AE": "ARE", "AD": "AND", "AG": "ATG", "AF": "AFG", "AI": "AIA", "VI": "VIR", "IS": "ISL", "IR": "IRN", "AM": "ARM", "AL": "ALB", "AO": "AGO", "AQ": "ATA", "AS": "ASM", "AR": "ARG", "AU": "AUS", "AT": "AUT", "AW": "ABW", "IN": "IND", "AX": "ALA", "AZ": "AZE", "IE": "IRL", "ID": "IDN", "UA": "UKR", "QA": "QAT", "MZ": "MOZ"}



def isIPRange(Start, End, Test):
    if ((ipaddress.IPv4Network(Start) <= ipaddress.IPv4Network(Test)) and (ipaddress.IPv4Network(End) >= ipaddress.IPv4Network(Test))):
#        print("Good")
        return True
    return False


def getASN(ipIp):
    beginIP = ipIp.split(".")
    firstTwo = beginIP[0]+"."+beginIP[1]

    with open(asnFile, 'r') as f:
        for line in f.readlines():
            if line.startswith(firstTwo):
                tempArray = line.split("\t")
                if isIPRange(tempArray[0], tempArray[1], ipIp):
                    return tempArray[2:]

    return [0, "BF", "BF", 0, 0]



def parseCounter(dat, num, raw):
    counter = Counter(sorted(dat))
    top = counter.most_common(num)

    tempString = ""
    for x in top:
        if isinstance(x, tuple):
            if (x[0] == ""):
                tempString = tempString + "\t" + str(x[1]) + " - " + "UNKNOWN" + "\n"
            else:
                tempString = tempString + "\t" + str(x[1]) + " - " + x[0] + "\n"
        else:
            if (x[0] == ""):
                tempString = tempString + "\t" + "UNKNOWN" + "\n"
            else:
               tempString = tempString + "\t" + x[0] + "\n"

    if raw:
        return tempString.replace('\t','').replace(' ', ',')
    else:
        return tempString


def initialBeep():
    file1 = open(logsFile, 'r')
    Lines = file1.readlines()

    topMethods = []
    topUserAgents = []
    topURI = []
    topIPs = []


    count = 0
    # Strips the newline character
    for line in Lines:
        count += 1
    #    print("Line{}: {}".format(count, line.strip()))
        data = json.loads(line)
        topMethods.append(data['method'])
        topUserAgents.append(data['user_agent'])
        topURI.append(data['uri'])
        topIPs.append(data['address'])



    print("\nTop Methods:\n", parseCounter(topMethods, entriesLimit, False))
    print("\nTop User Agents:\n", parseCounter(topUserAgents, entriesLimit, False))
    print("\nTop Pages:\n", parseCounter(topURI, entriesLimit, False))
    #print("\nTop IPs:\n", parseCounter(topIPs, 999, True))

    return parseCounter(topIPs, entriesLimit, True)



def ipParse(IpList):
    Countries_Raw = {}
    print("================================================================")
    print("IP Parsing: ")


    for line in IpList.splitlines():
        array = line.split(",")
        ipAddress = array[len(array)-1]
        ArrayArray = getASN(ipAddress)

        print(array[0], "-", ipAddress, ArrayArray[1], ArrayArray[2])

        found = False
        tmpNum = 0
    #    print(Countries_Raw)
        for x, y in Countries_Raw.items():
            if x == ArrayArray[1]:
                found = True
                tmpNum = y

        if not found:
            Countries_Raw[ArrayArray[1]] = int(array[0])
        else:
            newNum = (int(tmpNum)+int(array[0]))
            Countries_Raw[ArrayArray[1]] = newNum
    return Countries_Raw

def plotStart(Countries_Raw):
    print("================================================================")
    print("Plot Data")

    newCounts = []
    newNums = []

    maxNum = 0

    for x, y in Countries_Raw.items():
        found = False
        for xx, yy in IISO_Three.items():
            if (x == xx):
#                print(yy)
                newCounts.append(yy)

    for x, y in Countries_Raw.items():
#    print(y)
        newNums.append(y)
        if maxNum < y:
            maxNum = y

    print(newCounts)
    print(newNums)



    colour_1 = "rgb(190, 226, 222)"
    colour_2 = "rgb(27, 173, 172)"
    colour_3 = "rgb(0, 93, 100)"

    colourscale = [[0, colour_1],
                   [0.2, colour_2],
                   [1, colour_3]]



    def custom(countries, data, colorDat, nLevel):
        line_width = 2
        outline_params = dict(marker=dict(line=dict(
            color=colorDat,
            width=line_width
        )))
        out = dict(
            type='choropleth',
            locations=countries,
            locationmode='ISO-3',
            colorscale=colourscale,
            z=data,
            zmin=0,
            zmax=maxNum,
            name=nLevel)
        out.update(outline_params)
        return out

    #con = ["GBR", "USA", "DEU", "NOR", "ROU", "ISL", "PRT", "FRA"]
    #dat = [40,21,13,11,9,1,1,1]

    # newCounts = []
    # newNums = []



    lvl_One = [newCounts[0]]
    dat_One = [newNums[0]]
    lvl_Two = [newCounts[1]]
    dat_Two = [newNums[1]]
    lvl_Three = [newCounts[2]]
    dat_Three = [newNums[2]]
    lvl_Rst = newCounts[3:]
    dat_Rst = newNums[3:]



#    custom_data = custom(con,dat)

    lvlData_One = custom(lvl_One, dat_One, 'rgb(255, 0, 0)', "Top #1")
    lvlData_Two = custom(lvl_Two, dat_Two, 'rgb(0, 255, 0)', "Top #2")
    lvlData_Three = custom(lvl_Three, dat_Three, 'rgb(0, 0, 255)', "Top #3")
    lvlData_Rst = custom(lvl_Rst, dat_Rst, 'rgb(55, 55, 55)', "Rest")

    print(lvl_Rst)
    print(dat_Rst)

    layout = dict(
        geo={'scope': 'world', 'showcountries': True},
        title='Connections to Site',
        title_x=0.5,
        showlegend=True)

#    map = go.Figure(data=[data_p0, data_p2, data_p1, data_p3], layout=layout)
    map = go.Figure(data=[lvlData_One, lvlData_Two, lvlData_Three, lvlData_Rst], layout=layout)
    py.plot(map, filename='/config/plot.html', auto_open=False)



while True:
    datdat = initialBeep()
    crcr = ipParse(datdat)
    plotStart(crcr)
    time.sleep(timeSlep)
