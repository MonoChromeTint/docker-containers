
# Repo with info about Containers

## log2html
* DEPRICATED. Moved to pylog2html

## pylog2html
* Uses the python docker image to convert nginx data to plot
* Uses 'ip2asn-v4.tsv' graciously obtained from https://iptoasn.com/


## Building
```
git clone Repo
sudo docker-compose up --force-recreate --build --remove-orphans -d
sudo docker tag <HASH> {Domain}/{username}/{image_name}:{version_id}
```

## Pushing
```
sudo docker push {Domain}/{username}/{image_name}:{version_id}
```

## Pulling
```
sudo docker pull {Domain}/{username}/{image_name}:{version_id}
```
