
# Visualizing NGINX Logs With Plotly
The code has two portions:
* A) NGINX outputting logs in a json format
* B) The docker/standalone python script

## Setting up - NGINX
The only difference to NGINX is to have a custom logging format

For the nginx config file
```

http {

...


    log_format logger-json escape=json '{"source": "nginx", "time": $msec, "resp_body_size": $body_bytes_sent, "host": "$http_host", "address": "$remote_addr", "request_length": $request_length, "method": "$request_method", "uri": "$request_uri", "status": $status,  "user_agent": "$http_user_agent", "resp_time": $request_time, "upstream_addr": "$upstream_addr"}';
    access_log /config/log/nginx/access.log logger-json;


...

}
```


## Setting up the docker container

```
git clone <this repo>
cd ./docker-containers/pylog2html
docker-compose up --force-recreate --build --remove-orphans -d
```


Side note:
> By default, it uses 'access.log' as the main file to build the map off of, hence you'll need to copy that file from the nginx server.


### View the logs
```
docker logs <pylog2html name>
```



## Periodic creation
### Add a cron job
```
crontab -e
```

Add the following:
```
*/29 * * * * path/to/updater.sh
*/32 * * * * path/to/updater.sh "upload"
```

