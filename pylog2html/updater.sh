#!/bin/bash

NGINX_SERVER_HOSTNAME=""
NGINX_LOGS_LOCATION=""
NGINX_SAVE_LOCATION=""
PLOT_SAVE_LOCATION=""
PLOT_UPLOAD_LOCATION=""


## Example values
#NGINX_SERVER_HOSTNAME="HS-VPS-01"
#NGINX_LOGS_LOCATION="docker/000_Rev-Pro/Data/log/nginx/access.log"
#NGINX_SAVE_LOCATION="/home/user/analytics/11_logs2html/Data/"
#PLOT_SAVE_LOCATION="/home/user/analytics/11_logs2html/Data/plot.html"
#PLOT_UPLOAD_LOCATION="/home/user/docker/gohugo/Data/static/post/"




if [[ -z NGINX_SERVER_HOSTNAME ]] || [[ -z "$NGINX_LOGS_LOCATION" ]] || [[ -z "$NGINX_SAVE_LOCATION" ]] || [[ -z "$PLOT_SAVE_LOCATION" ]] || [[ -z "$PLOT_UPLOAD_LOCATION" ]] ; then
	echo "ERROR! Change default values!"
	exit
fi

if [[ "$1" != "upload" ]]; then
	scp "$NGINX_SERVER_HOSTNAME":"$NGINX_LOGS_LOCATION" "$NGINX_SAVE_LOCATION"
else
	scp "$PLOT_SAVE_LOCATION" "$NGINX_SERVER_HOSTNAME":"$PLOT_UPLOAD_LOCATION"
fi
